[![vaemoi logo](https://vaemoi.co/assets/img/svg/vaemoi_mark.svg)](https://vaemoi.co)

# vaemoi

#### A fosst (free open source software team) that's about change.
---

You're code, your bugs, your vision, your team is always *changing* vaemoi wants to help capture and make those changes visible | meaningful | trackable | *, to push you and your team to new heights. Be future-ready not future-proof
